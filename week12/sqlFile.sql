explain  select * from proteins where accession = "Q9UN86";
explain select * from proteins where pid like "%HUMAN";

create index idx1 on proteins(accession);
create index idx2 on proteins(pid);

/* alter table proteins drop index idn2 ; <--  how to delete index   */
